# Welcome to my CDK TypeScript project, the steps are followed by:  

This is a project for CDK development with TypeScript.

Step 1.-Open the new folder in Visual Studio Code Editor and open TerminalStep 

Step 2.-Create the app: Create the Directory- mkdir codewhisperer and cd codewhisperer

Step 3.-Initialize the CDK with cdk init app --language typescript

Step 4.-Import the module for aws service being created-Link

step 5.-Use TS code to create S3 bucket

Step 7.-Bootstrap (One Time) with cdk bootstrap

Step 8.-Synthesize an AWs CloudFormation template for the app with cdk synthStep 

Step 9.-Deploying the stack with cdk deploy
