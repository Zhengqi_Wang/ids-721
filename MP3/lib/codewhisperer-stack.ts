import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as s3 from 'aws-cdk-lib/aws-s3'; // 导入aws-s3库

class MyCdkProjectStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // 创建一个新的 S3 桶
    new s3.Bucket(this, 'IDS721bucketS3', { 
      versioned: true, // 启用版本控制
      encryption: s3.BucketEncryption.S3_MANAGED, // 使用 S3 管理的密钥启用加密
      // 这里可以配置其他属性，比如公共访问阻止等
    });
  }
}

export { MyCdkProjectStack };
